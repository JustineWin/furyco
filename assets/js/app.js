$('.companies-carousel').slick({
  responsive: [
  	{
      breakpoint: 1600,
      settings: 'unslick'
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 5,
        autoplay: true,
        infinite: true,
        speed: 500,
        dots: true,
        arrows: false,
        slidesToScroll: 5
      }
    },
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 4,
        autoplay: true,
        infinite: true,
        speed: 500,
        dots: true,
        arrows: false,
        slidesToScroll: 4
      }
    },
    {
      breakpoint: 700,
      settings: {
        slidesToShow: 3,
        autoplay: true,
        infinite: true,
        speed: 500,
        dots: true,
        arrows: false,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 550,
      settings: {
        slidesToShow: 2,
        autoplay: true,
        infinite: true,
        speed: 500,
        dots: true,
        arrows: false,
        slidesToScroll: 2
      }
    }
  ]
});

$('.testimonial-carousel').slick({
  centerMode: false,
  slidesToShow: 1,
  slidesToScroll: 1,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 5000,
  dots: true,
  arrows: false
});


$('.footer-companies-carousel').slick({
  dots: false,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 2000,
  speed: 300,
  slidesToShow: 8,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 6,
        autoplay: true,
        infinite: true,
        speed: 500,
        dots: false,
        slidesToScroll: 6
      }
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 5,
        autoplay: true,
        infinite: true,
        speed: 500,
        dots: false,
        slidesToScroll: 5
      }
    },
    {
      breakpoint: 630,
      settings: {
        slidesToShow: 4,
        autoplay: true,
        infinite: true,
        speed: 500,
        dots: false,
        slidesToScroll: 4
      }
    },
    {
      breakpoint: 580,
      settings: {
        slidesToShow: 3,
        autoplay: true,
        infinite: true,
        speed: 500,
        dots: false,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 450,
      settings: {
        slidesToShow: 2,
        autoplay: true,
        infinite: true,
        speed: 500,
        dots: false,
        arrows: false,
        slidesToScroll: 2
      }
    }
  ]
});

if ($(window).width() < 1200) {
   $('.custom-pills').removeClass('nav-stacked');
}
else {
   $('.custom-pills').addClass('nav-stacked');
}

